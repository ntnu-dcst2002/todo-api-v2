# Todo REST API 2.0

A simple Todo REST API with Node and Express.

### Instructions

1. Download the project:

```
git clone https://gitlab.com/ntnu-dcst2002/todo-api-v2.git
```

2. Install dependencies with npm:

```
cd todo-api-v2
npm install
```

3. Create a MySQL-database and run the following script:

```sql
CREATE TABLE IF NOT EXISTS Tasks (
  id INTEGER NOT NULL,
  title TEXT NOT NULL,
  done BOOLEAN,
  PRIMARY KEY (id));
```
4. Create a database configuration file called "config.js" in the root directory:

```javascript
process.env.MYSQL_HOST = '...';
process.env.MYSQL_USER= '...';
process.env.MYSQL_PASSWORD = '...';
process.env.MYSQL_DATABASE = '...';
```

5. Run the API:

```
npm run start
```